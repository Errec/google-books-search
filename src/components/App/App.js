// Components
import Header from '../../containers/Header/Header.js'
import Main from '../../containers/Main/Main.js'

// Styles
import './App.sass';

function App() {
  return (
    <div className="App">
      <Header/>
      <Main/>
    </div>
  );
}

export default App;
