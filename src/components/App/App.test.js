import { render, screen } from '@testing-library/react';
import App from './App';

test('renders google books search title', () => {
  render(<App />);
  const textElement = screen.getByText(/google books search/i);
  expect(textElement).toBeInTheDocument();
});
