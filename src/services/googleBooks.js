// Api
import { ApiGoogleBooks } from '../utils/api.js';

class googleBooksService {
    static getSearchResults(data) {
        console.log("googleBooks Service", data);
        const searchText = data.searchText.trim().replace(/\s/g, "+");

        return ApiGoogleBooks.get(`/volumes?q=${searchText}`);
    }
}

export default googleBooksService;
