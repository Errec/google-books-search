/* Modules */
import { combineReducers } from 'redux';

/* Reducers */
import googleBooksReducer from './googleBooks/reducer.js';

// all the reducers are in one place
const rootReducers = combineReducers({
    googleBooksReducer: googleBooksReducer,
});

export default rootReducers;
