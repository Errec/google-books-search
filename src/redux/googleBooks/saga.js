/* Modules */
import { call, put, takeEvery } from 'redux-saga/effects';

import googleBooksService from '../../services/googleBooks.js';

/* Types */
import { FETCH_GOOGLE_BOOKS_SEARCH } from './types';
import { SET_SEARCH_TEXT } from './types';

function* fetchGoogleBooksSearch(action) {
    const response = yield call(googleBooksService.getSearchResults, action.data);
    console.log("Saga Action: ", action);
    console.log('Response: ', response);
    if (response.status === 200) {
        yield put({ type: FETCH_GOOGLE_BOOKS_SEARCH.SUCCESS, googleBooksSearchData: response.data });
    } else {
        yield put({ type: FETCH_GOOGLE_BOOKS_SEARCH.FAILURE, googleBooksSearchError: response.data.error });
    }
}

function* setSearchText(action) {
    yield put({ type: SET_SEARCH_TEXT.SUCCESS, SearchText: action.data.searchText });
}

export const googleBooksSaga = [
    takeEvery(FETCH_GOOGLE_BOOKS_SEARCH.REQUEST, fetchGoogleBooksSearch),
    takeEvery(SET_SEARCH_TEXT.REQUEST, setSearchText),
];
