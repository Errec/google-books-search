// Types
import {
    FETCH_GOOGLE_BOOKS_SEARCH,
    SET_SEARCH_TEXT,
} from './types.js';

const INITIAL_STATE = {
    googleBooksResult: {
        data: [],
        isLoading: false,
        error: '',
        enableSearch: false,
    },
    SearchText: {
        data: '',
    },
};

const googleBooksReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SET_SEARCH_TEXT.REQUEST:
            return {
                ...state,
                SearchText: {
                    ...state.SearchText,
                },
            };

        case SET_SEARCH_TEXT.SUCCESS:
            return {
                ...state,
                SearchText: {
                    data: action.SearchText,
                },
        };

        case FETCH_GOOGLE_BOOKS_SEARCH.REQUEST:
            return {
                ...state,
                googleBooksResult: {
                    ...state.googleBooksResult,
                    isLoading: true,
                },
            };
            
        case FETCH_GOOGLE_BOOKS_SEARCH.SUCCESS:
            return {
                ...state,
                googleBooksResult: {
                    data: action.googleBooksSearchData,
                    isLoading: false,
                    enableSearch: true,
                },
            };
            
        case FETCH_GOOGLE_BOOKS_SEARCH.FAILURE:
            return {
                ...state,
                googleBooksResult: {
                    data: [],
                    isLoading: false,
                    error: action.googleBooksSearchError,
                    enableSearch: true,
                },
            };
                    
        case FETCH_GOOGLE_BOOKS_SEARCH.RESET:
            return {
                ...state,
                googleBooksResult: INITIAL_STATE.googleBooksResult,
            };

        default:
            return state;
    }
};

export default googleBooksReducer;
