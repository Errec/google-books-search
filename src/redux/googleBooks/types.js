/* Create action types */
import createActionTypes from '../createActionTypes.js';

export const FETCH_GOOGLE_BOOKS_SEARCH = createActionTypes('FETCH_SOF_SEARCH');
export const SET_SEARCH_TEXT = createActionTypes('SET_SEARCH_TEXT');
