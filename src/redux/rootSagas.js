// Modules
import { all } from 'redux-saga/effects';

// Saga
import { googleBooksSaga } from './googleBooks/saga.js';

function* rootSagas() {
  yield all([
    ...googleBooksSaga,
  ]);
}

export default rootSagas;
