import SearchInput from '../../components/SearchInput/SearchInput.js'

const Main = () => {
    return (
        <main>
            <SearchInput/>
        </main>
    )
}

export default Main
